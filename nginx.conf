
worker_processes 1;

events {
	worker_connections 1024;
}

http {
	map $sent_http_content_type $cache_control_custom {
		default		'';
		text/html	'no-cache';
	}

	server {
		listen 80;
		server_name localhost;

		root /usr/share/nginx/html;
		index index.html index.htm;
		include /etc/nginx/mime.types;

		gzip on;
		gzip_min_length 1000;
		gzip_proxied expired no-cache no-store private auth;
		gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;

		add_header Cache-Control $cache_control_custom;

		# don't send the nginx version number in error pages and Server header
		server_tokens off;
		add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-Content-Type-Options "nosniff";
		add_header Referrer-Policy "no-referrer";
		# Content-Security-Policy section
		set $default_src "default-src 'self';";
		set $script_src "script-src 'self' 'unsafe-inline' cloudheader-uat.azureedge.net cloudheader-prd.azureedge.net cdnjs.cloudflare.com 35qzgjdq9bss.statuspage.io cdn4.mxpnl.com;";
		set $style_src "style-src 'self' 'unsafe-inline' cloudheader-uat.azureedge.net cloudheader-prd.azureedge.net ckncdnlive.azureedge.net;";
		set $font_src "font-src 'self' data: ckncdnlive.azureedge.net;";
		set $img_src "img-src 'self' data: clarksonscloud-uat.azureedge.net clarksonscloud.azureedge.net ckncdnlive.azureedge.net *.sea.live sealogin-trfm-stg-cdn.azureedge.net sealogin-trfm-int-cdn.azureedge.net sealogin-trfm-dev-cdn.azureedge.net sealogin-trfm-cdn.azureedge.net;";
		set $connect_src "connect-src 'self' *.sea.live dc.services.visualstudio.com *.service.signalr.net;";
		set $frame_src "frame-src 'self' *.sea.live 35qzgjdq9bss.statuspage.io;";
		set $report_uri "report-uri /api/csp-report";
		set $scp "$default_src $script_src $style_src $font_src $img_src $connect_src $frame_src upgrade-insecure-requests; block-all-mixed-content; $report_uri";
		add_header Content-Security-Policy $scp;

		location / {
			try_files $uri $uri/ /index.html;
		}
	}
}