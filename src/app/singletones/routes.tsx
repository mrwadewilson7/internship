import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';

import { HomePage } from '../../containers/pages/homePage/homePage';
import { PreviewPage } from '../../containers/pages/previewPage/previewPage';

class RouteConfig {
  public static readonly default: string = '/';
  public static readonly preview: string = '/preview';
  public static previewId: string = '/preview/:id';
  public static previewParamsFilter: string = '/preview?';
}

class Routes extends React.Component {
  render(): React.ReactNode {
    return (
      <Switch>
        <Route exact path={RouteConfig.default} component={HomePage} />
        <Route exact path={RouteConfig.preview} component={PreviewPage} />
        <Route exact path={RouteConfig.previewId} component={PreviewPage} />
        <Route exact path={RouteConfig.previewParamsFilter} component={PreviewPage} />
        <Route path={RouteConfig.default} component={(): JSX.Element => <div>404</div>} />
      </Switch>
    );
  }
}

const ConnectedRoutes = connect()(Routes);
export default ConnectedRoutes;
