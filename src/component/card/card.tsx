/* eslint-disable @typescript-eslint/no-unsafe-call */
import React, { FC } from 'react';

import { useHistory } from 'react-router';

import { IRegistry } from '../../store/modules/characters/character.types';

import styles from './card.module.css';

export const Card: FC<IRegistry> = (props: IRegistry) => {
  const history = useHistory();
  const { imageURL, name, gender, race, side } = props;
  return (
    <div
      className={styles.card}
      onClick={() => {
        history.push(`/preview/${props.id}`);
        props.handleOpenModal();
      }}
    >
      <img className={styles.image} src={imageURL} alt="card" />
      <div className={styles.card__info}>
        <p className={styles.name}>{name}</p>

        <div>
          <div className={styles.card__description}>
            <div className={styles.text}>{'Пол'}</div>
            <div className={styles.text}>{gender}</div>
          </div>
          <hr className={styles.card__line} />
          <div className={styles.card__description}>
            <div className={styles.text}>{'Раса'}</div>
            <div className={styles.text}>{race}</div>
          </div>
          <hr className={styles.card__line} />
          <div className={styles.card__description}>
            <div className={styles.text}>{'Сторона'}</div>
            <div className={styles.text}>{side}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
