/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import React, { FC } from 'react';

import styles from './filterBar.module.css';
import { IFilterBarProps } from './filterBar.type';
import { FilterCategories } from './filterCategories/filterCategories';

export const FilterBar: FC<IFilterBarProps> = (props): JSX.Element => {
  const data = Object.keys(props.filters);
  return (
    <div className={styles.filter__bar}>
      {data.map((el, idx) => (
        <div className={styles.filter__type} key={idx}>
          <FilterCategories
            filters={{ [el]: props.filters[el] }}
            filterChangeForm={props.filterChangeForm}
            filterParams={{ [el]: props.filterParams[el] }}
          />
        </div>
      ))}
    </div>
  );
};
