import { IFiltersData } from '../../containers/pages/previewPage/previewPage.types';
import { ICheckboxFilterByComponent, IDictionary } from '../../store/modules/filters/filters.type';

export interface IFilterProps {
  [key: string]: IDictionary[];
}
export interface IFilterBarProps {
  filters: IFilterProps;
  filterChangeForm: (data: IFiltersData) => void;
  filterParams: ICheckboxFilterByComponent;
}
