import React, { FC } from 'react';

import { Form, Field } from 'react-final-form';

import { IDictionary } from '../../../store/modules/filters/filters.type';
import { useAppSelector } from '../../../store/store.hooks';
import { IFilterBarProps } from '../filterBar.type';

import styles from './filterBlock.module.css';
import { IFilterBlockValues } from './filterBlock.types';

export const FilterBlock: FC<IFilterBarProps> = (props): JSX.Element => {
  const formData = useAppSelector((state) => state.filter.filters);
  const fieldName: string = Object.keys(props.filters)[0];
  const dataArray: IDictionary[] = props.filters[fieldName];
  const handleSubmit = (values: IFilterBlockValues): void => {
    props?.filterChangeForm({ [fieldName]: values[fieldName] });
  };

  return (
    <div className={styles.filter__drop}>
      <Form
        initialValues={{
          ...formData,
        }}
        onSubmit={(values): void => handleSubmit(values)}
      >
        {(reactFinalProps): React.ReactElement => (
          <form className={styles.form} onChange={(): unknown => reactFinalProps.handleSubmit()}>
            {dataArray.map(
              (el): JSX.Element => (
                <label className={styles.filter__check} key={el.id}>
                  <Field name={fieldName} component="input" type="checkbox" value={el.id} />
                  {el.value}
                </label>
              )
            )}
          </form>
        )}
      </Form>
    </div>
  );
};
