export interface IFilterBlockValues {
  [key: string]: string[];
}
