/* eslint-disable @typescript-eslint/restrict-template-expressions*/
import React, { FC, useState } from 'react';

import { IFilterBarProps } from '../filterBar.type';

import { FilterBlock } from '../filterBlock/filterBlock';

import styles from './filterCategories.module.css';
import { FilterHead, ArrowButton } from './testElem';

export const FilterCategories: FC<IFilterBarProps> = (props): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className={styles.filter__selector}>
      <FilterHead isOpen={isOpen} onClick={(): void => setIsOpen(!isOpen)}>
        <div className={styles.filter__name}>
          {Object.values(props.filterParams)[0]?.length !== 0
            ? `Choose: ${Object.values(props.filterParams)[0]?.length}`
            : Object.keys(props.filters)[0]}
        </div>
        <ArrowButton isOpen={isOpen} width="14" height="8" viewBox="0 0 14 8" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.8136 0.195244C13.5651 -0.0650667 13.1622 -0.0651111 12.9137 0.195289L7.00015 6.39053L1.08634 0.195244C0.837858 -0.0650667 0.434912 -0.0651111 0.186391 0.195289C-0.0621303 0.455644 -0.0621303 0.877734 0.186391 1.13809L6.5502 7.80476C6.66954 7.92978 6.83139 8 7.00015 8C7.16891 8 7.33081 7.92973 7.4501 7.80471L13.8136 1.13804C14.0621 0.877734 14.0621 0.4556 13.8136 0.195244Z" />
        </ArrowButton>
      </FilterHead>
      {isOpen ? <FilterBlock {...props} /> : null}
    </div>
  );
};
