export interface ISelector {
  isOpen: boolean;
  width: string;
  height: string;
  viewBox: string;
  xmlns: string;
}
