/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import styled from 'styled-components';

import { ISelector } from './test.types';

export const ArrowButton = styled.svg<ISelector>`
  fill: ${(props): string => (props.isOpen ? '#3FC4FD' : '#ffffff')};
  width: ${(props): string => props.width};
  height: ${(props): string => props.height};
  viewbox: ${(props): string => props.viewBox};
  xmlns: ${(props): string => props.xmlns};
`;
export const FilterHead = styled.div<{ isOpen: boolean }>`
  border: ${(props): string => (props.isOpen ? '1px solid #3FC4FD' : '1px solid #ffffff')};
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 140px;
  height: 45px;
  box-sizing: border-box;
  padding: 0 11px 0 14px;
  border-radius: 4px;
  cursor: pointer;
`;
