/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */

import React, { FC } from 'react';

import styles from './modal.module.css';

export const ModalWindow: FC<any> = ({ children, handleClose }): JSX.Element => {
  return (
    <div className={styles.modal} onClick={handleClose} id={'modalId'}>
      {children}
    </div>
  );
};
