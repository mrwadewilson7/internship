export interface IModal {
  handleClose: () => void;
}
