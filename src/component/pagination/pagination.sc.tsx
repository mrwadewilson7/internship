import styled from 'styled-components';

import { IEllipse } from './pagination.types';

export const Ellipse = styled.ellipse<IEllipse>`
  fill: ${(props: IEllipse): string => (props.el === props.currentPage ? '#3FC4FD' : '#ffffff')};
  cx: ${(props: IEllipse): string => props.cx};
  cy: ${(props: IEllipse): string => props.cy};
  rx: ${(props: IEllipse): string => props.rx};
  ry: ${(props: IEllipse): string => props.ry};
  cursor: pointer;
`;
