import React, { FC } from 'react';

import styles from './pagination.module.css';
import { Ellipse } from './pagination.sc';
import { IPagination } from './pagination.types';

export const Pagination: FC<IPagination> = (props): JSX.Element => {
  let paginationArray: number[] = [];
  for (let i = 1; i <= props.maxPages; i++) {
    paginationArray = [...paginationArray, i];
  }

  const maxPageFromRender = (): number => {
    let page: number;
    const dif: number = props.maxPages - 2;
    if (props.currentPage > dif) {
      page = props.maxPages - props.limitElements;
      return page;
    } else if (props.currentPage <= Math.ceil(props.limitElements / 2) || props.maxPages === props.limitElements) {
      page = 0;
      return page;
    } else {
      page = (props.currentPage as number) - (props.limitElements - Math.floor(props.limitElements / 2));
      return page;
    }
  };

  return (
    <div className={styles.pagination}>
      <svg
        onClick={(): void => props.handleChangePage('-')}
        width="12"
        height="20"
        viewBox="0 0 12 20"
        fill="#ffffff"
        cursor="pointer"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M11.3843 19.5845C11.7502 19.2272 11.7425 18.656 11.3667 18.3088L2.42796 10.0471L11.1375 1.54368C11.5035 1.18639 11.4957 0.615216 11.12 0.268012C10.7443 -0.0791924 10.1431 -0.0709693 9.77712 0.28638L0.40491 9.4369C0.22915 9.60849 0.132267 9.83928 0.135539 10.0785C0.138811 10.3177 0.242033 10.5458 0.42242 10.7125L10.0414 19.6028C10.417 19.95 11.0182 19.9418 11.3843 19.5845Z"
          fill="white"
        />
        <path
          d="M11.3843 19.5845C11.7502 19.2272 11.7425 18.656 11.3667 18.3088L2.42796 10.0471L11.1375 1.54368C11.5035 1.18639 11.4957 0.615216 11.12 0.268012C10.7443 -0.0791924 10.1431 -0.0709693 9.77712 0.28638L0.40491 9.4369C0.22915 9.60849 0.132267 9.83928 0.135539 10.0785C0.138811 10.3177 0.242033 10.5458 0.42242 10.7125L10.0414 19.6028C10.417 19.95 11.0182 19.9418 11.3843 19.5845Z"
          stroke="black"
        />
      </svg>
      {paginationArray.slice(maxPageFromRender(), maxPageFromRender() + props.limitElements).map((el, idx) => {
        return (
          <svg onClick={(): void => props.handleChangePage(el)} key={idx} width="9" height="10" viewBox="0 0 9 10">
            {el}
            <Ellipse
              el={el}
              currentPage={props.currentPage as number}
              cx="4.6457"
              cy="4.9695"
              rx="4.17402"
              ry="4.15395"
            />
          </svg>
        );
      })}

      <svg
        onClick={(): void => props.handleChangePage('+')}
        width="12"
        height="20"
        viewBox="0 0 12 20"
        fill="#ffffff"
        cursor="pointer"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M0.882613 0.264198C0.511807 0.616447 0.511743 1.18767 0.882676 1.53998L9.70763 9.92311L0.882613 18.3067C0.511807 18.6589 0.511743 19.2301 0.882676 19.5824C1.25355 19.9348 1.8548 19.9348 2.22567 19.5824L11.7222 10.561C11.9002 10.3918 12.0003 10.1624 12.0003 9.92311C12.0003 9.68387 11.9002 9.45437 11.7221 9.28525L2.22561 0.264257C1.8548 -0.0881119 1.25348 -0.0881119 0.882613 0.264198Z"
          fill="white"
        />
        <path
          d="M0.882613 0.264198C0.511807 0.616447 0.511743 1.18767 0.882676 1.53998L9.70763 9.92311L0.882613 18.3067C0.511807 18.6589 0.511743 19.2301 0.882676 19.5824C1.25355 19.9348 1.8548 19.9348 2.22567 19.5824L11.7222 10.561C11.9002 10.3918 12.0003 10.1624 12.0003 9.92311C12.0003 9.68387 11.9002 9.45437 11.7221 9.28525L2.22561 0.264257C1.8548 -0.0881119 1.25348 -0.0881119 0.882613 0.264198Z"
          stroke="black"
        />
      </svg>
    </div>
  );
};
