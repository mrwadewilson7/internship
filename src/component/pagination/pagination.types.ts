export interface IPagination {
  handleChangePage: (data: number | string) => void;
  minPage: number;
  maxPages: number;
  currentPage: number | string;
  limitElements: number;
}
export interface IEllipse {
  el: number;
  currentPage: number;
  cx: string;
  cy: string;
  rx: string;
  ry: string;
}
