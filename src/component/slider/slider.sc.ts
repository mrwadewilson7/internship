/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import styled from 'styled-components';

import { ICard } from './slider.types';

export const CardBox = styled.div<ICard>`
  transition: 1s;
  transform: translateX(-${(props: ICard): number => ((props.currentPage as number) - 1) * 1305}px);
  key: ${(props: ICard): number => props.key};
`;
