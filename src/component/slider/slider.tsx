/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import React, { FC } from 'react';

import { Card } from '../card/card';

import styles from './slider.module.css';
import { CardBox } from './slider.sc';
import { ISliderProps } from './slider.types';

export const Slider: FC<ISliderProps> = ({ list, currentPage, limitElements, handleOpenModal }): React.ReactElement => {
  return (
    <div>
      <div className={styles.list__characters}>
        {list?.map((el, idx) => {
          return (
            <CardBox key={idx} currentPage={currentPage}>
              <Card {...el} handleOpenModal={handleOpenModal} />
            </CardBox>
          );
        })}
      </div>
    </div>
  );
};
