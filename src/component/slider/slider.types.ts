import { IRegistry } from '../../store/modules/characters/character.types';

export interface ISliderProps {
  currentPage: number | string;
  list: IRegistry[] | undefined;
  limitElements: number;
  handleOpenModal: () => void;
}
export interface ICard {
  key: number;
  currentPage: number | string;
}
