import FormControl from '@material-ui/core/FormControl';
import { Search } from '@material-ui/icons';
import { TextField, InputAdornment } from '@mui/material';

import { styled } from '@mui/material/styles';
import React, { FC } from 'react';

import { Form, Field } from 'react-final-form';

import styles from './textFieldHandler.module.css';
import { IFilterInputProps } from './textFieldHandler.types';

const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: 'white',
  },
  '& label': {
    color: '#3D3D3D',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '18px',
    lineHeight: '21px',
  },
  '& input': {
    color: 'white',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '18px',
    lineHeight: '21px',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: 'white',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: 'white',
    },
    '&:hover fieldset': {
      borderColor: 'white',
    },
    '&.Mui-focused fieldset': {
      borderColor: 'white',
    },
  },
});

export const TextFieldHandler: FC<IFilterInputProps> = (props): JSX.Element => {
  const handleChange = (values: string): void => {
    props.handleChange(values);
  };
  const formData = props.formData;
  return (
    <Form
      initialValues={{
        formData,
      }}
      onSubmit={(values): void => handleChange(values.search)}
    >
      {(reactFinalProps): React.ReactElement => (
        <form onChange={reactFinalProps.handleSubmit}>
          <Field name="search">
            {(props): JSX.Element => (
              <FormControl className={styles.textFieldControl}>
                <CssTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  label="Search heroes"
                  variant="outlined"
                  name={props.input.name}
                  value={formData || ''}
                  onChange={props.input.onChange}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Search className={styles.icon} fontSize="large" />
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>
            )}
          </Field>
        </form>
      )}
    </Form>
  );
};
