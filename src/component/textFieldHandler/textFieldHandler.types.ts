export interface IFilterInputProps {
  formData: string;
  handleChange: (text: string) => void;
}
