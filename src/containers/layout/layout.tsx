import React, { CSSProperties, FC, useCallback } from 'react';

import { Header } from '../../core/component/header/header';

import { TLayoutProps } from './layout.types';

export const Layout: FC<TLayoutProps> = ({ style, children }) => {
  const getStyle = useCallback(
    (): CSSProperties => ({
      display: 'flex',
      flexDirection: 'column',
      width: '100vw',
      height: '100vh',
      ...style,
    }),
    [style]
  );

  return (
    <div id={'layout'} style={getStyle()}>
      <Header />
      {children}
    </div>
  );
};
