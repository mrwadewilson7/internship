import { CSSProperties } from 'react';

export interface ILayoutProps {
  style?: CSSProperties;
}

export type TLayoutProps = ILayoutProps;
