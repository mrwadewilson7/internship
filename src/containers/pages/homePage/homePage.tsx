import React, { FC } from 'react';

import { NavLink } from 'react-router-dom';

import styles from './homePage.module.css';

export const HomePage: FC = (): JSX.Element => {
  return (
    <div className={styles.home}>
      <h1 className={styles.home__title}>Find all your favorite heroes “Star Wars”</h1>
      <h3 className={styles.home__description}>
        You can know the type of heroes, its strengths, disadvantages and abilities
      </h3>
      <NavLink className={styles.home__start} to={'/preview'}>
        Start
      </NavLink>
    </div>
  );
};
