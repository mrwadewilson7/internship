import qs from 'qs';
import React, { FC, useEffect } from 'react';

import { useHistory, useParams } from 'react-router';

import { FilterBar } from '../../../component/filterBar/filterBar';
import { ModalWindow } from '../../../component/modal/modal';
import { Pagination } from '../../../component/pagination/pagination';
import { Slider } from '../../../component/slider/slider';
import { TextFieldHandler } from '../../../component/textFieldHandler/textFieldHandler';
import { ModalCard } from '../../../core/component/modalCard/modalCard';
import { ModalForm } from '../../../core/component/modalForm/modalForm';

import { characterActions } from '../../../store/modules/characters/character.slice';
import { filtersActions } from '../../../store/modules/filters/filters.slice';
import { ICheckboxFilterByComponent } from '../../../store/modules/filters/filters.type';
import { paginationActions } from '../../../store/modules/pagination/pagination.slice';
import { useAppDispatch, useAppSelector } from '../../../store/store.hooks';

import { paginationCalculation } from '../../../utils/pagination';

import styles from './previewPage.module.css';
import { IFiltersData } from './previewPage.types';

// eslint-disable-next-line sonarjs/cognitive-complexity
export const PreviewPage: FC = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const currentPage = useAppSelector((state) => state.pagination.currentPage);
  const dataFromStore = useAppSelector((state) => state.filter);
  useEffect(() => {
    dispatch(filtersActions.setFiltersByServer(['gender', 'race', 'side']));
  }, []);
  useEffect(() => {
    let queryParams = { filters: {}, search: '' };
    queryParams = { ...queryParams, ...qs.parse(window.location.search.slice(1)) };

    const dataToStore = {
      ...dataFromStore,
      filters: { ...dataFromStore.filters, ...queryParams.filters },
      search: '' || queryParams.search,
    };
    if (queryParams.filters || queryParams.search) {
      dispatch(filtersActions.setFilterByUrl(dataToStore));
    } else {
      dispatch(characterActions.fetchRegistryData(1));
    }
  }, []);
  useEffect(() => {
    id
      ? id !== 'new'
        ? dispatch(characterActions.setOpenModal({ isOpen: true, isCharacterModal: true }))
        : dispatch(characterActions.setOpenModal({ isOpen: true, isCharacterModal: false }))
      : null;
  }, []);
  const isOpen = useAppSelector((state) => state.characters.isOpen);
  const isCharacterModal = useAppSelector((state) => state.characters.isCharacterModal);
  const listChar = useAppSelector((state) => state.characters.characters);
  let filterParams = useAppSelector((state) => state.filter.filters);
  const dictionaries = useAppSelector((state) => state.filter.dictionary);
  const filterInput = useAppSelector((state) => state.filter.search);
  const limitCharacters: number = 3;
  const filterChangeForm = (data: IFiltersData): void => {
    filterParams = { ...filterParams, ...data } as ICheckboxFilterByComponent;
    dispatch(filtersActions.setFilterByCheckbox(filterParams));
  };
  const handleChange = (data: string): void => {
    dispatch(filtersActions.setInputFilter(data || ''));
  };
  const handleOpenModal = () => {
    dispatch(characterActions.setOpenModal({ isOpen: true, isCharacterModal: true }));
  };
  const handleCloseModal = () => {
    history.push('/preview');
    dispatch(characterActions.setOpenModal({ isOpen: false, isCharacterModal: false }));
    dispatch(characterActions.setClearCharacter());
  };

  const handleAddNewPerson = () => {
    history.push('/preview/new');
    dispatch(characterActions.setOpenModal({ isOpen: true, isCharacterModal: false }));
  };

  const pageLimit = paginationCalculation(listChar, 3);
  dispatch(paginationActions.setMaxPages(pageLimit));
  const paginationInfo = useAppSelector((state) => state.pagination);
  const handleChangePage = (data: string | number): void => {
    let page = paginationInfo.currentPage as number;
    if (data === '-' && page !== 1) {
      page = page - 1;
    } else if (data === '+' && page !== paginationInfo.endPage) {
      page = page + 1;
    } else if (typeof data === 'number') {
      page = data;
    }
    dispatch(paginationActions.setCurrentPage(page));
  };

  return (
    <section className={styles.preview}>
      <div className={styles.preview__block}>
        <h1 className={styles.preview__head}>Who's Your Favorite Star Wars character</h1>
        <div className={styles.preview__filter}>
          <TextFieldHandler formData={filterInput} handleChange={handleChange} />
        </div>
        <div className={styles.filter_and_add}>
          <FilterBar filters={dictionaries} filterChangeForm={filterChangeForm} filterParams={filterParams} />
          <div className={styles.newPerson} onClick={handleAddNewPerson}>
            <p className={styles.newPersonText}>Add</p>
            <p className={styles.newPersonText}>+</p>
          </div>
        </div>
        <div className={styles.slider}>
          <Slider
            currentPage={currentPage}
            list={listChar}
            limitElements={limitCharacters}
            handleOpenModal={handleOpenModal}
          />
          <Pagination
            handleChangePage={handleChangePage}
            minPage={paginationInfo.startPage}
            maxPages={paginationInfo.endPage}
            currentPage={paginationInfo.currentPage}
            limitElements={limitCharacters}
          />
        </div>
      </div>
      {isOpen ? (
        <ModalWindow handleClose={handleCloseModal}>
          {isCharacterModal ? (
            <ModalCard handleClose={handleCloseModal} />
          ) : (
            <ModalForm handleClose={handleCloseModal} />
          )}
        </ModalWindow>
      ) : null}
    </section>
  );
};
