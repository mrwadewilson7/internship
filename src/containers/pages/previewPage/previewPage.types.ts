export interface IFiltersData {
  [key: string]: string[];
}
