import React, { FC } from 'react';

import Logo from '../../../assets/images/logo.png';

import { Navbar } from '../navbar/navbar';

import styles from './header.module.css';

export const Header: FC = (): JSX.Element => {
  return (
    <header className={styles.header}>
      <div className={styles.menu}>
        <img className={styles.logo} src={Logo} alt="Star-Wars Logo" />
        <Navbar />
      </div>
    </header>
  );
};
