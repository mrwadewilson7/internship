/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import React, { FC, useEffect } from 'react';

import { useParams } from 'react-router';

import { characterActions } from '../../../store/modules/characters/character.slice';
import { ICharacter } from '../../../store/modules/characters/character.types';
import { useAppDispatch, useAppSelector } from '../../../store/store.hooks';

import styles from './modalCard.module.css';
import { IModalHandler } from './modalCard.types';

export const ModalCard: FC<IModalHandler> = (props): JSX.Element => {
  const dispatch = useAppDispatch();
  const params = useParams<{ id: string }>();

  useEffect(() => {
    dispatch(characterActions.setCharacter(params.id));
  }, []);
  const character = useAppSelector((state) => state.characters.character) as ICharacter;
  return (
    <div
      className={styles.modalCard}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <div className={styles.closeButton} onClick={props.handleClose}>
        <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M16.5 30.25C24.0939 30.25 30.25 24.0939 30.25 16.5C30.25 8.90608 24.0939 2.75 16.5 2.75C8.90608 2.75 2.75 8.90608 2.75 16.5C2.75 24.0939 8.90608 30.25 16.5 30.25Z"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M20.625 12.375L12.375 20.625"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M12.375 12.375L20.625 20.625"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </div>
      <div>
        <img className={styles.image} src={character?.imageURL} alt="" />
        <div className={styles.tags}>
          {character?.tag1 !== null ? <div className={styles.tagsItem}>{character?.tag1}</div> : null}
          {character?.tag2 !== null ? <div className={styles.tagsItem}>{character?.tag2}</div> : null}
          {character?.tag3 !== null ? <div className={styles.tagsItem}>{character?.tag3}</div> : null}
        </div>
      </div>
      <div>
        <p className={styles.header}>{character?.name}</p>
        <div className={styles.textBlock}>
          <p className={styles.text}>Gender</p>
          <p className={styles.text}>{character?.gender.value}</p>
        </div>
        <hr className={styles.lines} />
        <div className={styles.textBlock}>
          <p className={styles.text}>Race</p>
          <p className={styles.text}>{character?.race.value}</p>
        </div>
        <hr className={styles.lines} />
        <div className={styles.textBlock}>
          <p className={styles.text}>Side</p>
          <p className={styles.text}>{character?.side.value}</p>
        </div>
        <hr className={styles.lines} />
        <p className={styles.description}>{character?.description}</p>
      </div>
    </div>
  );
};
