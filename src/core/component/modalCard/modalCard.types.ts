export interface IModalHandler {
  handleClose: () => void;
}
