/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import React, { FC } from 'react';

import { Form, Field } from 'react-final-form';

import { ICharacter, ICharacterForm } from '../../../../store/modules/characters/character.types';
import { useAppDispatch, useAppSelector } from '../../../../store/store.hooks';

import styles from './leftModalForm.module.css';
import { characterActions } from '../../../../store/modules/characters/character.slice';
import { IModalProps } from './leftModalForm.types';

export const LeftModalForm: FC<IModalProps> = (props) => {
  const formData = props.formData;
  const dispatch = useAppDispatch();

  const handleSendToStore = (values: any) => {
    const formValues: ICharacterForm = {
      name: values.name,
      imageURL: values.imageURL,
      nameColor: values.nameColor,
      backgroundColor: values.backgroundColor,
      parametersColor: values.parametersColor,
      description: values.description,
      gender: values.gender,
      race: values.race,
      side: values.side,
      tag1: values.tag1,
    };
    props.handleChange(formValues);
  };
  return (
    <Form
      initialValues={{
        ...formData,
      }}
      onSubmit={(values): void => handleSendToStore(values)}
    >
      {(reactFinalProps): React.ReactElement => (
        <form onChange={reactFinalProps.handleSubmit}>
          <div className={styles.formBox}>
            <div className={styles.formName}>
              <label className={styles.formName}>
                <p>Add name</p>
                <Field
                  className={styles.formText}
                  name="name"
                  component="input"
                  type="text"
                  value={formData.name || ''}
                />
              </label>
            </div>
            <div className={styles.formTypes}>
              <label>
                <p>Add gender</p>
                <Field className={styles.gender} name="gender" component="input" value={formData.gender || ''} />
              </label>

              <label>
                <p>Add race</p>
                <Field className={styles.race} name="race" component="input" value={formData.race || ''} />
              </label>

              <label>
                <p>Add side</p>
                <Field className={styles.side} name="side" component="input" value={formData.side || ''} />
              </label>
            </div>
            <div className={styles.formName}>
              <label className={styles.formName}>
                <p>Add description</p>
                <Field
                  className={styles.textAreaDescription}
                  name="description"
                  component="textarea"
                  type="textarea"
                  value={formData.description || ''}
                />
              </label>
            </div>
            <div className={styles.formName}>
              <label className={styles.formName}>
                <p>Add tag</p>
                <Field
                  className={styles.formText}
                  name="tag1"
                  component="input"
                  type="text"
                  value={formData.tag1 || ''}
                />
              </label>
            </div>
            <div className={styles.bottom}>
              <div className={styles.imgBox}>
                <div className={styles.imgBlock}>
                  <img className={styles.img} src={`${formData.imageURL}`} />
                </div>
                <Field
                  className={styles.urlInput}
                  name="imageURL"
                  component="input"
                  type="text"
                  value={formData.imageURL || ''}
                />
              </div>
              <div className={styles.colorBox}>
                <label className={styles.colorBlock}>
                  <Field
                    className={styles.color}
                    name="nameColor"
                    component="input"
                    type="color"
                    value={formData.nameColor}
                  />
                  <p className={styles.text}>Name Color</p>
                </label>
                <label className={styles.colorBlock}>
                  <Field
                    className={styles.color}
                    name="backgroundColor"
                    component="input"
                    type="color"
                    value={formData.backgroundColor}
                  />
                  <p className={styles.text}>Background Color</p>
                </label>
                <label className={styles.colorBlock}>
                  <Field
                    className={styles.color}
                    name="parametersColor"
                    component="input"
                    type="color"
                    value={formData.parametersColor}
                  />
                  <p className={styles.text}>Parameter color</p>
                </label>
              </div>
            </div>
          </div>
        </form>
      )}
    </Form>
  );
};
