import { ICharacterForm } from '../../../../store/modules/characters/character.types';

export interface IModalProps {
  handleChange: (value: ICharacterForm) => void;
  formData: ICharacterForm;
}
