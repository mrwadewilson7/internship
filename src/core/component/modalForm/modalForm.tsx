/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import React, { FC } from 'react';

import { IModalHandler } from '../modalCard/modalCard.types';

import { LeftModalForm } from './leftModalForm/leftModalForm';
import styles from './modalForm.module.css';
import { RightModalForm } from './rightModalForm/rightModalForm';
import { useAppDispatch, useAppSelector } from '../../../store/store.hooks';
import { characterActions } from '../../../store/modules/characters/character.slice';
import { ICharacterForm } from '../../../store/modules/characters/character.types';

export const ModalForm: FC<IModalHandler> = (props): JSX.Element => {
  const dispatch = useAppDispatch();
  const formData = useAppSelector((state) => state.characters.characterForm);
  const handleChange = (value: ICharacterForm) => {
    dispatch(characterActions.setCharacterForm(value));
  };
  return (
    <div
      className={styles.modalForm}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <div className={styles.closeButton} onClick={props.handleClose}>
        <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M16.5 30.25C24.0939 30.25 30.25 24.0939 30.25 16.5C30.25 8.90608 24.0939 2.75 16.5 2.75C8.90608 2.75 2.75 8.90608 2.75 16.5C2.75 24.0939 8.90608 30.25 16.5 30.25Z"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M20.625 12.375L12.375 20.625"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M12.375 12.375L20.625 20.625"
            stroke="white"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </div>
      <div className={styles.partModalFirst}>
        <LeftModalForm handleChange={handleChange} formData={formData} />
      </div>

      <div className={styles.partModalSecond}>
        <RightModalForm />
      </div>
    </div>
  );
};
