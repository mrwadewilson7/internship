import { ICharacterForm } from '../../../store/modules/characters/character.types';

export interface IModalFormHandler {
  handleClose: () => void;
}
