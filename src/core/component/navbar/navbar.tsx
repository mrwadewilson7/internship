import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';

import styles from './navbar.module.css';
import { INavbarLinkTypes, NavbarPaths } from './navbar.types';

export const Navbar: FC = (): JSX.Element => {
  const navigationElements: INavbarLinkTypes[] = [
    { name: 'Home', path: NavbarPaths.Home },
    { name: 'Preview', path: NavbarPaths.Preview },
  ];
  return (
    <div className={styles.menu}>
      {navigationElements.map((el, idx) => (
        <NavLink className={styles.menu__item} key={idx} to={el.path}>
          {el.name}
        </NavLink>
      ))}
    </div>
  );
};
