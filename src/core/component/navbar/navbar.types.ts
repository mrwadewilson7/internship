export enum NavbarPaths {
  Home = '/',
  Preview = '/preview',
}

export interface INavbarLinkTypes {
  name: string | number;
  path: NavbarPaths;
}
