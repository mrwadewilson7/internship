import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import { ConnectedRouter } from 'connected-react-router';
import history from './app/singletones/history';
import { Layout } from './containers/layout/layout';
import ConnectedRoutes from './app/singletones/routes';

const MOUNT_NODE = document.getElementById('root');

const renderApp = (): void => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Layout>
          <ConnectedRoutes />
        </Layout>
      </ConnectedRouter>
    </Provider>,
    MOUNT_NODE
  );
};
renderApp();
