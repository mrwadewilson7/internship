export const filters = {
  Gender: ['Men', 'Women', 'Undefined'],
  Race: ['Droid', 'Human', 'Wookiee', 'Undefined'],
  Side: ['Darkness', 'Light'],
};
