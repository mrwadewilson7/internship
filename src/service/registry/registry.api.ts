/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unsafe-assignment*/

import { ICharacter, IContentRegistry, IRegistry } from '../../store/modules/characters/character.types';
import { IDictionary } from '../../store/modules/filters/filters.type';
import { restGet } from '../requestApi';

export const fetchRegistry = async () => {
  const response = await restGet(`http://localhost:5000/api/STAR_WARS/character`);
  return response.data as IRegistry;
};

export const fetchDictionary = async (names: string[]) => {
  let data: IDictionary[] = [];
  for (const el of names) {
    const response = await restGet(`http://localhost:5000/api/STAR_WARS/${el}`);
    data = { ...data, [el]: [...response.data] };
  }
  return data;
};

export const fetchFilteredRegistry = async (params: string) => {
  const response = await restGet(`http://localhost:5000/api/STAR_WARS/character?${params}`);
  return response.data as IContentRegistry;
};

export const fetchCharacterWithId = async (params: string) => {
  const response = await restGet(`http://localhost:5000/api/STAR_WARS/character/${params}`);
  return response.data as ICharacter;
};
