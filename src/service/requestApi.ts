import axios, { AxiosResponse } from 'axios';

export const restGet = (url: string): Promise<AxiosResponse> => {
  return axios.get(url);
};

export const restPost = (url: string, data?: unknown): Promise<AxiosResponse> => {
  return axios.post(url, data);
};
