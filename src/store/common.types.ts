export interface IAction<TypeName> {
  type: TypeName;
}

export interface IStrictAction<TypeName, DataType> extends IAction<TypeName> {
  payload: DataType;
}

export interface ISoftAction<TypeName, DataType> extends IAction<TypeName> {
  payload?: DataType;
}
