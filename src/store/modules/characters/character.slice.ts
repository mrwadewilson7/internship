/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { ICharacter, ICharacterForm, ICharacterMassive, IContentRegistry, IModalInfo } from './character.types';
import { IDictionary } from '../filters/filters.type';

const initialState: ICharacterMassive = {
  characters: [],
  page: 1,
  minPage: 1,
  maxPages: 1,
  character: {
    name: '',
    imageURL: '',
    nameColor: '#FFFFFF',
    backgroundColor: '#FFFFFF',
    parametersColor: '#FFFFFF',
    description: '',
    gender: { id: '', value: '' },
    race: { id: '', value: '' },
    side: { id: '', value: '' },
    tag1: '',
    tag2: '',
    tag3: '',
  },
  characterForm: {
    name: '',
    imageURL: '',
    nameColor: '#FFFFFF',
    backgroundColor: '#FFFFFF',
    parametersColor: '#FFFFFF',
    description: '',
    gender: '',
    race: '',
    side: '',
    tag1: '',
  },
  isOpen: false,
  isCharacterModal: false,
};

const characterSlice = createSlice({
  name: 'Characters',
  initialState,
  reducers: {
    fetchRegistryData: (state, action: PayloadAction<number>) => {
      state.page = action.payload;
    },
    setCharacterList: (state, action: PayloadAction<IContentRegistry>) => {
      const data = action.payload;
      const { content } = data;
      state.characters = content;
    },
    setCharacterForm: (state, action: PayloadAction<ICharacterForm>) => {
      state.characterForm = action.payload;
    },
    setOpenModal: (state, action: PayloadAction<IModalInfo>) => {
      state.isOpen = action.payload.isOpen;
      state.isCharacterModal = action.payload.isCharacterModal;
    },
    setCharacter: (state, action: PayloadAction<string>) => {
      return;
    },
    setClearCharacter: (state) => {
      state.character = initialState.character;
    },
    setCharacterById: (state, action: PayloadAction<ICharacter>) => {
      state.character = action.payload;
    },
    setCurrentPage: (state, action: PayloadAction<number>) => {
      state.page = action.payload;
    },
    setCurrentPageByArrow: (state, action: PayloadAction<string>) => {
      if (action.payload === '-') {
        if (state.page === 1) {
          state.page = 1;
        } else {
          state.page = --state.page;
        }
      } else {
        if (state.page === 3) {
          state.page = 3;
        } else {
          state.page = ++state.page;
        }
      }
    },
  },
});

export default characterSlice;
export const characterReducers = characterSlice.reducer;
export const characterActions = characterSlice.actions;
