import { IDictionary } from '../filters/filters.type';
import { filtersActions } from '../filters/filters.slice';

export interface ICharacterMassive {
  characters: IRegistry[];
  page: number;
  minPage: number;
  maxPages: number;
  character: ICharacter;
  characterForm: ICharacterForm;
  isOpen: boolean;
  isCharacterModal: boolean;
}

export interface IRegistry {
  id: string;
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  gender: string;
  race: string;
  side: string;
  handleOpenModal: () => void;
}
export interface ICharacter {
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  description: string | null;
  gender: IDictionary;
  race: IDictionary;
  side: IDictionary;
  tag1: string | null;
  tag2: string | null;
  tag3: string | null;
}

export interface ICharacterForm {
  name: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  description: string | null;
  gender: string;
  race: string;
  side: string;
  tag1: string | null;
}

export interface IContentRegistry {
  content: IRegistry[];
}

export interface IModalInfo {
  isOpen: boolean;
  isCharacterModal: boolean;
}
export type ICharacterActionTypes = ReturnType<typeof filtersActions.setFilterByCheckbox>;
