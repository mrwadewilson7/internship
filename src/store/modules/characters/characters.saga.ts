import qs from 'qs';
import { all, call, put, select, takeLatest } from 'typed-redux-saga';

import { fetchCharacterWithId, fetchFilteredRegistry, fetchRegistry } from '../../../service/registry/registry.api';

import { TAppState } from '../../store.types';
import { sendToUrl } from '../filters/filters.saga';
import { filtersActions } from '../filters/filters.slice';

import { IFilter } from '../filters/filters.type';

import { characterActions } from './character.slice';
import { ICharacter, IContentRegistry } from './character.types';

export const filterSelector = (state: TAppState): IFilter => state.filter;

export function* fetchRegistryDataSaga(): Generator<any, any, any> {
  const registryData: IContentRegistry = yield call(fetchRegistry);

  yield put(characterActions.setCharacterList(registryData));
}
export function* fetchFilteredCharacter(
  action: ReturnType<typeof filtersActions.setFilterByUrl>
): Generator<any, any, any> {
  const dataToSend = action.payload;
  let queryParams = {};
  if (!dataToSend.search) {
    queryParams = { ...dataToSend.filters };
  } else {
    queryParams = { ...dataToSend.filters, values: dataToSend.search };
  }

  const filteredPersons: IContentRegistry = yield* call(fetchFilteredRegistry, qs.stringify(queryParams));
  yield put(characterActions.setCharacterList(filteredPersons));
}
export function* fetchCharacter(action: ReturnType<typeof characterActions.setCharacter>): Generator<any, any, any> {
  try {
    const data: ICharacter = yield* call(fetchCharacterWithId, action.payload);
    yield put(characterActions.setCharacterById(data));
  } catch (error) {
    return null;
  }
}
export function* fetchFilteredCharacterByComponent(): Generator<any, any, any> {
  const filters: IFilter = yield* select(sendToUrl);
  let queryParams = {};
  if (filters.search.length === 0) {
    queryParams = { ...filters.filters };
  } else {
    queryParams = { ...filters.filters, values: filters.search };
  }

  const filteredPersons: IContentRegistry = yield* call(fetchFilteredRegistry, qs.stringify(queryParams));
  yield put(characterActions.setCharacterList(filteredPersons));
}
export default function* (): Generator<any, any, any> {
  yield all([
    takeLatest(characterActions.fetchRegistryData, fetchRegistryDataSaga),
    takeLatest(filtersActions.setFilterByUrl, fetchFilteredCharacter),
    takeLatest(filtersActions.setFilterByCheckbox, fetchFilteredCharacterByComponent),
    takeLatest(filtersActions.setInputFilter, fetchFilteredCharacterByComponent),
    takeLatest(characterActions.setCharacter, fetchCharacter),
  ]);
}
