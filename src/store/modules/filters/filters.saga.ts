import qs from 'qs';
import { all, call, put, select, takeLatest } from 'typed-redux-saga';

import { fetchDictionary } from '../../../service/registry/registry.api';
import { TAppState } from '../../store.types';

import { filtersActions } from './filters.slice';
import { ICheckboxFilterByComponent, IDictionary, IFilter } from './filters.type';

export const filterSelection = (filters: ICheckboxFilterByComponent, search: string): void => {
  let queryParams;
  if (search.length === 0) {
    queryParams = { filters };
  } else {
    queryParams = { filters, search };
  }
  if (
    filters.gender?.length === 0 &&
    filters.race?.length === 0 &&
    filters.side?.length === 0 &&
    search.length === 0 &&
    search.length === 0
  ) {
    window.history.pushState('', '', `/preview`);
  } else {
    window.history.pushState('', '', `/preview?${qs.stringify(queryParams)}`);
  }
};
export const sendToUrl = (state: TAppState): IFilter => state.filter;

export function* CurrentFilterCheckboxSaga(): Generator<any, any, any> {
  try {
    const filters: IFilter = yield* select(sendToUrl);
    filterSelection(filters.filters, filters.search);
  } catch (error) {
    return null;
  }
}

// eslint-disable-next-line sonarjs/no-identical-functions
export function* CurrentInputSaga(): Generator<any, any, any> {
  try {
    const filters: IFilter = yield* select(sendToUrl);
    filterSelection(filters.filters, filters.search);
  } catch (error) {
    return null;
  }
}

export function* fetchDictionaryByServer(
  action: ReturnType<typeof filtersActions.setFiltersByServer>
): Generator<any, any, any> {
  try {
    const dictionaryNames = action.payload;
    const dictionaries = yield call(fetchDictionary, dictionaryNames);
    yield* put(filtersActions.setDictionary(dictionaries));
  } catch (error) {
    return null;
  }
}
export default function* (): unknown {
  yield all([
    takeLatest(filtersActions.setFilterByCheckbox, CurrentFilterCheckboxSaga),
    takeLatest(filtersActions.setInputFilter, CurrentInputSaga),
    takeLatest(filtersActions.setFiltersByServer, fetchDictionaryByServer),
  ]);
}
