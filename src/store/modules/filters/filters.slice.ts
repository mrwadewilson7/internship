import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { ICheckboxFilterByComponent, IFilter } from './filters.type';

const initialState: IFilter = {
  filters: {
    gender: [],
    race: [],
    side: [],
  },
  dictionary: {},
  search: '',
};

const filtersSlice = createSlice({
  name: 'Filters',
  initialState,
  reducers: {
    setFilterByCheckbox: (state, action: PayloadAction<ICheckboxFilterByComponent>) => {
      state.filters = action.payload;
    },
    setDictionary: (state, action: PayloadAction<ICheckboxFilterByComponent>) => {
      state.dictionary = action.payload;
    },
    setInputFilter: (state, action: PayloadAction<string>) => {
      state.search = action.payload;
    },
    setFiltersByServer: (state, action: PayloadAction<string[]>) => {
      return;
    },
    setFilterByUrl: (state, action: PayloadAction<IFilter>) => {
      state.filters = action.payload.filters;
      state.search = action.payload.search;
    },
  },
});

export default filtersSlice;
export const filtersReducers = filtersSlice.reducer;
export const filtersActions = filtersSlice.actions;
