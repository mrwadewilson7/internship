import { filtersActions } from './filters.slice';

export interface IFilter {
  filters: ICheckboxFilterByComponent;
  dictionary: ICheckboxFilterByComponent;
  search: string;
}
export interface ICheckboxFilterByComponent {
  [key: string]: IDictionary[];
}
export interface IDictionary {
  id: string;
  value: string;
}
export type IFilterActionTypes =
  | ReturnType<typeof filtersActions.setFilterByCheckbox>
  | ReturnType<typeof filtersActions.setInputFilter>
  | ReturnType<typeof filtersActions.setFilterByUrl>
  | ReturnType<typeof filtersActions.setFiltersByServer>;
