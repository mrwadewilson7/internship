import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IPagination } from './pagination.types';

const initialState: IPagination = {
  currentPage: 1,
  startPage: 1,
  endPage: 1,
};

const paginationSlice = createSlice({
  name: 'Pagination',
  initialState,
  reducers: {
    setCurrentPage: (state, action: PayloadAction<number | string>) => {
      state.currentPage = action.payload;
    },
    setMaxPages: (state, action: PayloadAction<number>) => {
      state.endPage = action.payload;
    },
  },
});

export default paginationSlice;
export const paginationReducers = paginationSlice.reducer;
export const paginationActions = paginationSlice.actions;
