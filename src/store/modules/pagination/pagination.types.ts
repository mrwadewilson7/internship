export interface IPagination {
  currentPage: number|string;
  startPage: number;
  endPage: number;
}
