import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import history from '../app/singletones/history';

import { characterReducers } from './modules/characters/character.slice';
import { filtersReducers } from './modules/filters/filters.slice';
import { paginationReducers } from './modules/pagination/pagination.slice';

const rootReducer = combineReducers({
  form: formReducer,
  filter: filtersReducers,
  characters: characterReducers,
  pagination: paginationReducers,
  router: connectRouter(history),
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;
