import { all } from 'redux-saga/effects';

import watchRegistry from './modules/characters/characters.saga';
import watchFilters from './modules/filters/filters.saga';

export default function* rootSaga(): Generator<unknown> {
  yield all([watchFilters(), watchRegistry()]);
}
