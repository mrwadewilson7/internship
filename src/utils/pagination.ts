import { IRegistry } from '../store/modules/characters/character.types';

export const paginationCalculation = (characterList: IRegistry[] | undefined, limit: number): number => {
  let calcMaxPage = 1;
  if (characterList) {
    const { length } = characterList;
    calcMaxPage = Math.ceil(length / limit);
  }
  return calcMaxPage;
};
