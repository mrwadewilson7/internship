import { IRegistry } from '../store/modules/characters/character.types';
import { ICheckboxFilterByComponent } from '../store/modules/filters/filters.type';

export const registryDoubleFiltrationByParameters = (
  targetForFiltering: IRegistry[] | undefined,
  filterSelector: ICheckboxFilterByComponent,
  filterSearchParam: string
): IRegistry[] => {
  const regexp = new RegExp(filterSearchParam, 'i');
  if (filterSelector.Gender?.length !== 0 || filterSelector.Race?.length !== 0 || filterSelector.Side?.length !== 0) {
    targetForFiltering = targetForFiltering?.filter((el) => {
      console.log(el);
      // return (
      //   filterSelector.Gender?.includes(el.gender) ||
      //   filterSelector.Race?.includes(el.race) ||
      //   filterSelector.Side?.includes(el.side)
      // );
    });
  }

  if (filterSearchParam) {
    targetForFiltering = targetForFiltering?.filter((el) => {
      if (filterSearchParam?.length !== 0) {
        return (
          regexp.test(el.gender.toLowerCase()) ||
          regexp.test(el.race.toLowerCase()) ||
          regexp.test(el.side.toLowerCase()) ||
          regexp.test(el.name.toLowerCase())
        );
      }
    });
  }
  return targetForFiltering as IRegistry[];
};
